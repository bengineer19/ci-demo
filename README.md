# CI demo

Demonstration of CI/CD for Hackaday.

This example takes the application in `web/logic.py` and exposes it as an API using `web/api.py`.